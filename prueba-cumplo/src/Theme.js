const MainTheme = {
    palette: {
      primary: {
        light: "#284E56",
        main: "#284E56",
        dark: "#284E56",
        contrastText: "#71F1B2",
      },
      secondary: {
        light: "#005159",
        main: "#005159",
        dark: "#005159",
        contrastText: "#71F1B2",
      },
    },
    typography:{
      fontFamily:[
        "sans-serif!important"
      ]
    }
  };
  export default MainTheme;
  

