import { useTheme } from "@mui/material/styles";
import {
  Brush,
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

export default ({ data, showBrush = false }) => {
  const theme = useTheme();
  return (
    <LineChart
      width={500}
      height={200}
      data={data}
      syncId="anyId"
      margin={{
        top: 10,
        right: 30,
        left: 0,
        bottom: 0,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="fecha" />
      <YAxis />
      <Tooltip />
      <Line
        type="monotone"
        dataKey="dato"
        stroke={theme.palette.primary.main}
        fill={theme.palette.primary.contrastText}
      />
      {showBrush && <Brush />}
    </LineChart>
  );
};
