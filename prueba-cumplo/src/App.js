
import "./App.css";

import { createTheme, ThemeProvider } from "@mui/material/styles";
import MainTheme from "./Theme";
import Home from "./modules/Home";
import { CssBaseline } from "@mui/material";

function App() {
  const defaultTheme = createTheme({
    ...MainTheme,
    spacing: (factor) => `${0.5 * factor}rem`,
  });

  return (
    <div className="App" >
      <ThemeProvider theme={defaultTheme}>
        <CssBaseline />
        <Home />
      </ThemeProvider>
    </div>
  );
}

export default App;
