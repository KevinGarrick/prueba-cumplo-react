import {
  Grid,
  List,
  ListItem,
  ListItemText,
  Paper,
  Table,
  TableBody,
  TableCell,
  tableCellClasses,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import axios from "axios";
import DateRangePicker from "@mui/lab/DateRangePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { useEffect, useState } from "react";
import { Box } from "@mui/system";
import format from "date-fns/format";

import { styled } from "@mui/material/styles";
import LineChart from "../../components/LineChart";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

export default () => {
  const [value, setValue] = useState([null, null]);
  const [series, setSeries] = useState(null);
  const [tableData, setTableData] = useState(null);

  const getData = async () => {
    if (!value[0] && !value[1]) return;

    const params = {
      start_date: format(new Date(value[0]), "yyyy-MM-dd"),
      end_date: format(new Date(value[1]), "yyyy-MM-dd"),
    };
    const client = axios.create({
      baseURL: "/cumplo/api",
    });
    const result = await client.get("/dlls_n_udis", { params: params });
    setSeries(result?.data?.series);
    buildTableData(result?.data?.series);
  };

  const buildTableData = (data) => {
    const newData = {};

    data.map((item) => {
      const data = {};
      item.data.map((i) => {
        const date = i.fecha.split("/");
        data[`${date[2]}-${date[1]}-${date[0]}`] = i.dato;
      });
      newData[item.type] = data;
    });
    const dates = [];
    let currDate = value[0];
    do {
      dates.push(currDate.toISOString().slice(0, 10));
      currDate = shiftDate(currDate, 1);
    } while (currDate <= value[1]);
    newData["dates"] = dates;
    console.log(newData);
    setTableData(newData);
  };

  function shiftDate(date, numDays) {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + numDays);
    return newDate;
  }

  useEffect(() => {
    if (value[1] > value[0]) getData();
  }, [value]);
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} style={{ display: "grid", justifyItems: "center" }}>
        <Box style={{ width: "100%", paddingBottom: "1rem" }}>
          Selecciona el rango de fechas para vizualizar variaciones de Dolar y
          UDIS
        </Box>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DateRangePicker
            startText="Fecha inicial"
            endText="Fecha final"
            value={value}
            onChange={(newValue) => {
              setValue(newValue);
            }}
            renderInput={(startProps, endProps) => (
              <>
                <TextField
                  {...startProps}
                  style={{ backgroundColor: "white" }}
                />
                <Box sx={{ mx: 2 }}> a </Box>
                <TextField {...endProps} style={{ backgroundColor: "white" }} />
              </>
            )}
          />
        </LocalizationProvider>
      </Grid>
      <Grid item xs={4} style={{ display: "grid", justifyItems: "center" }}>
        <TableContainer component={Paper} elevation={2}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <StyledTableCell>Fecha</StyledTableCell>
                <StyledTableCell>UDIS</StyledTableCell>
                <StyledTableCell>Dolar</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {tableData &&
                tableData.dates.map((item, index) => (
                  <TableRow key={index}>
                    <TableCell>{item}</TableCell>
                    <TableCell>{tableData.udis[item]}</TableCell>
                    <TableCell>{tableData.dlls[item]}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      <Grid item xs={8}>
        <Grid container direction="column">
          {series &&
            series.map((item, index) => {
              return (
                <Grid item key={index}>
                  <Paper elevation={3}>
                    <p>{item.title}</p>
                    <LineChart data={item.data} />

                    <Grid container direction="row" spacing={3} style={{padding: "1rem 3rem"}}>
                      <Grid item>
                        <ListItemText
                          primary="Mínimo"
                          secondary={item.statistics.min.dato}
                        />
                      </Grid>

                      <Grid item>
                        <ListItemText
                          primary="Máximo"
                          secondary={item.statistics.max.dato}
                        />
                      </Grid>
                      <Grid item>
                        <ListItemText
                          primary="Promedio"
                          secondary={item.statistics.avg}
                        />
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              );
            })}
        </Grid>
      </Grid>
    </Grid>
  );
};
