import PropTypes from "prop-types";

import {
  Icon,
  Fab,
  Zoom,
  Container,
  AppBar,
  Toolbar,
  Typography,
  useScrollTrigger,
  Box,
  Button,
} from "@mui/material";
import UdisDlls from "../UdisDlls";
import { useState } from "react";
import TIIE from "../TIIE";

function ScrollTop(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector(
      "#back-to-top-anchor"
    );

    if (anchor) {
      anchor.scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
    }
  };

  return (
    <Zoom in={trigger}>
      <Box
        onClick={handleClick}
        role="presentation"
        sx={{ position: "fixed", bottom: 16, right: 16 }}
      >
        {children}
      </Box>
    </Zoom>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default (props) => {
  const [tiie, setTiie] = useState(false)

  const handleSetTiie = () =>{
    setTiie(!tiie)
  }

  return (
    <>
      
      <AppBar>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} align="left">
            Cumplo
          </Typography>
          <Button color="inherit" onClick={handleSetTiie}>{!tiie ? "TIIE" : "UDIS y Dolares" }</Button>
        </Toolbar>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
      <div style={{padding: "1rem 5rem"}}>
       {!tiie && <UdisDlls/>}
       {tiie && <TIIE/>}
      </div>
      <ScrollTop {...props}>
        <Fab color="primary" size="small" aria-label="scroll back to top">
          <Icon>keyboard_arrow_up</Icon>
        </Fab>
      </ScrollTop>
    </>
  );
};
