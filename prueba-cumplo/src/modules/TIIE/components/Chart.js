import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import { CircularProgress, Icon, Box } from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import LineChart from "../../../components/LineChart";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <Icon>close</Icon>
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default ({ open, onClose, selected }) => {
  const [data, setData] = useState(null);

  const getData = async () => {
    const client = axios.create({
      baseURL: "/cumplo/api",
    });
    const response = await client.get(`/tiies/${selected}`);
    setData(response.data);
  };

  useEffect(() => {
    if (selected) getData();
  }, [selected]);
  return (
    <div>
      <BootstrapDialog
        onClose={onClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullWidth
      >
        {!data && (
          <Box
            style={{
              display: "flex",
              justifyContent: "center",
              padding: "6rem",
            }}
          >
            <CircularProgress />
          </Box>
        )}
        {data && (
          <>
            <BootstrapDialogTitle
              id="customized-dialog-title"
              onClose={onClose}
            >
              {data.titulo}
            </BootstrapDialogTitle>
            <DialogContent dividers>
              <LineChart data={data.datos} showBrush />
            </DialogContent>
          </>
        )}
      </BootstrapDialog>
    </div>
  );
};
