import axios from "axios";
import { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import {
  Icon,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  tableCellClasses,
  TableHead,
  TableRow,
} from "@mui/material";
import Chart from "./components/Chart";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

export default () => {
  const [data, setData] = useState(null);
  const [open, setOpen] = useState(false);
  const [selected, setSelected] = useState(null)

  const handleOpen = (serieId) => {
    setSelected(serieId)
    setOpen(true);
  };
  
  const handleClose = () => {
      setOpen(false)
  };

  const getData = async () => {
    const client = axios.create({
      baseURL: "/cumplo/api",
    });
    const response = await client.get("/tiies");
    setData(response.data);
  };

  useEffect(() => {
    getData();
  }, []);
  if (!data) return <></>;

  return (
    <Paper elevation={3}>
      <Table>
        <TableHead>
          <TableRow>
            <StyledTableCell> </StyledTableCell>
            <StyledTableCell>Fecha</StyledTableCell>
            <StyledTableCell>Porcentaje</StyledTableCell>
            <StyledTableCell> </StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.series.map((item, index) => (
            <TableRow key={index}>
              <TableCell>{item.titulo}</TableCell>
              <TableCell>{item.datos[0].fecha}</TableCell>
              <TableCell>{item.datos[0].dato}</TableCell>
              <TableCell>
                <IconButton
                  onClick={() => handleOpen(item.idSerie)}
                  color="primary"
                  size="medium"
                >
                  <Icon>insights</Icon>
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Chart open={open} onClose={handleClose} selected={selected}/>
    </Paper>
  );
};
